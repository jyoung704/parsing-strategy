package parse;

import parse.strategy.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Parse {

    public static void main(String[] args) {

        List<String> absoluteFilePaths = getAbsoluteFilePathsToParse();

        parseAll(absoluteFilePaths);
    }

    private static List<String> getAbsoluteFilePathsToParse() {

        URL filesToParse = Parse.class.getResource("/files");
        Path pathOfFilesToParse = getPathOrNull(filesToParse);

        return getFilePaths(pathOfFilesToParse);
    }

    private static Path getPathOrNull(URL url) {
        try {
            return Paths.get(url.toURI());
        } catch (URISyntaxException | NullPointerException e) {
            String message = String.format("\nurl = %s", url);
            System.getLogger("ParseLogger").log(System.Logger.Level.ERROR, e + message);
            return null;
        }
    }

    private static List<String> getFilePaths(Path path) {
        try {
           return getAllSupportedFilePaths(path);
        } catch (IOException | NullPointerException e) {
            String message = String.format("\npath = %s", path);
            System.getLogger("ParseLogger").log(System.Logger.Level.ERROR, e + message);
            return new ArrayList<>();
        }
    }

    private static List<String> getAllSupportedFilePaths(Path path) throws IOException {
        List<String> filePaths = new ArrayList<>();
        Files.walk(path, 1)
                .map(Path::toString)
                .filter(Parse::fileTypeIsSupported)
                .forEach(filePaths::add);
        return filePaths;
    }

    private static boolean fileTypeIsSupported(String fileName) {
        return fileName.endsWith(".txt");
    }

    private static void parseAll(List<String> filePaths) {
        for (String filePath :
                filePaths) {
            parse(filePath);
        }
    }

    private static void parse(String absoluteFilePath) {

        ParseStrategy parseStrategy = findParseStrategyOrNull(absoluteFilePath);

        parseFileIfParseStrategyFound(absoluteFilePath, parseStrategy);
    }

    private static ParseStrategy findParseStrategyOrNull(String absoluteFilePath) {
        String fileName = absoluteFilePath.substring(absoluteFilePath.lastIndexOf('/') + 1);
        if (fileName.contains("space")) {
            return new SpaceParser();
        } else if (fileName.contains("comma")) {
            return new CommaParser();
        } else if (fileName.contains("pipe")) {
            return new PipeParser();
        }
        return null;
    }

    private static void parseFileIfParseStrategyFound(String absoluteFilePath, ParseStrategy parseStrategy) {
        if (parseStrategy != null) {
            ParserContext parserContext = new ParserContext(parseStrategy, absoluteFilePath);
            parserContext.printFormattedPeople();
        }
    }
}
