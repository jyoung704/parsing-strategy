package parse.util;

import parse.strategy.ParseStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class ParseUtils {

    public enum Field {
        LAST_NAME,
        FIRST_NAME,
        MIDDLE_INITIAL,
        GENDER,
        DATE_OF_BIRTH,
        FAVORITE_COLOR
    }

    public static List<String> getPersonPropertiesWithFormattedGender(ParseStrategy parser, List<String> personProperties) {
        int indexOfGender = parser.getInputPropertiesFormat().indexOf(Field.GENDER);
        String abbreviation = personProperties.get(indexOfGender);
        String fullGender = switch (abbreviation) {
            case "F" -> "Female";
            case "M" -> "Male";
            case "N" -> "Non-binary";
            default -> "N/A";
        };

        List<String> personPropertiesWithFormattedGender = new ArrayList<>(personProperties);
        personPropertiesWithFormattedGender.set(indexOfGender, fullGender);
        return personPropertiesWithFormattedGender;
    }

    public static List<String> getPersonPropertiesWithFormattedDate(ParseStrategy parser, List<String> personProperties) {
        int indexOfDate = parser.getInputPropertiesFormat().indexOf(Field.DATE_OF_BIRTH);
        String date = personProperties.get(indexOfDate);
        String formattedDate = date.replace('-', '/');

        List<String> personPropertiesWithFormattedDate = new ArrayList<>(personProperties);
        personPropertiesWithFormattedDate.set(indexOfDate, formattedDate);
        return personPropertiesWithFormattedDate;
    }

    public static List<String> getPersonPropertiesInOutputFormat(ParseStrategy parser,
                                                                 List<String> personProperties,
                                                                 List<Field> outputPropertiesFormat) {
        List<Field> inputPropertiesFormat = parser.getInputPropertiesFormat();

        return propertyIndexToOutputIndex(personProperties, inputPropertiesFormat, outputPropertiesFormat)
                .entrySet()
                .stream()
                .filter(ParseUtils::outputFormatContainsProperty)
                .sorted(Map.Entry.comparingByValue())
                .map(entry -> propertyFromPropertyIndex(entry, personProperties))
                .collect(Collectors.toList());
    }

    private static Map<Integer, Integer> propertyIndexToOutputIndex(List<String> personProperties,
                                                                    List<Field> inputPropertiesFormat,
                                                                    List<Field> outputPropertiesFormat) {

        return IntStream.range(0, personProperties.size())
                .boxed()
                .collect(Collectors.toMap(
                        i -> i,
                        i -> getIndexOfPropertyInOutputFormat(i, inputPropertiesFormat, outputPropertiesFormat)));
    }

    private static int getIndexOfPropertyInOutputFormat(int propertyIndex, List<Field> inputPropertiesFormat, List<Field> outputPropertiesFormat) {
        return outputPropertiesFormat.indexOf(inputPropertiesFormat.get(propertyIndex));
    }

    private static boolean outputFormatContainsProperty(Map.Entry<Integer, Integer> propertyIndexToOutputIndex) {
        return propertyIndexToOutputIndex.getValue() > -1;
    }

    private static String propertyFromPropertyIndex(Map.Entry<Integer, Integer> propertyIndexToOutputIndex, List<String> personProperties) {
        int propertyIndex = propertyIndexToOutputIndex.getKey();
        return personProperties.get(propertyIndex);
    }

    public static List<String> split(String person, String inputDelimiter) {
        String[] splitPerson = person.split(inputDelimiter);
        return Arrays.asList(splitPerson);
    }

    public static String formatPersonPropertiesForOutput(List<String> personArray) {
        return String.join(" ", personArray);
    }
}
