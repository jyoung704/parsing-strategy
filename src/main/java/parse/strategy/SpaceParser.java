package parse.strategy;

import java.util.Arrays;
import java.util.List;

import static parse.util.ParseUtils.*;
import static parse.util.ParseUtils.Field.*;

public class SpaceParser implements ParseStrategy {

    private static final List<Field> inputPropertiesFormat =
            Arrays.asList(
                    LAST_NAME,
                    FIRST_NAME,
                    MIDDLE_INITIAL,
                    GENDER,
                    DATE_OF_BIRTH,
                    FAVORITE_COLOR
            );
    private static final String inputDelimiter = " ";

    @Override
    public String getFormattedPerson(String unformattedPerson, List<Field> outputPropertiesFormat) {
        List<String> personProperties = split(unformattedPerson, getInputDelimiter());

        personProperties = getPersonPropertiesWithFormattedGender(this, personProperties);
        personProperties = getPersonPropertiesWithFormattedDate(this, personProperties);
        personProperties = getPersonPropertiesInOutputFormat(this, personProperties, outputPropertiesFormat);

        return formatPersonPropertiesForOutput(personProperties);
    }

    @Override
    public List<Field> getInputPropertiesFormat() {
        return inputPropertiesFormat;
    }

    @Override
    public String getInputDelimiter() {
        return inputDelimiter;
    }

}
