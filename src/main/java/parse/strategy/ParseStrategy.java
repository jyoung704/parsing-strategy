package parse.strategy;

import parse.util.ParseUtils.Field;
import java.util.List;

public interface ParseStrategy {
    String getFormattedPerson(String unformattedPerson, List<Field> outputPropertiesFormat);
    List<Field> getInputPropertiesFormat();
    String getInputDelimiter();
}
