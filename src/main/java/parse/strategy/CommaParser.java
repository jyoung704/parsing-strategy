package parse.strategy;

import java.util.Arrays;
import java.util.List;

import static parse.util.ParseUtils.*;
import static parse.util.ParseUtils.Field.*;

public class CommaParser implements ParseStrategy {

    private static final List<Field> inputPropertiesFormat =
            Arrays.asList(
                    LAST_NAME,
                    FIRST_NAME,
                    GENDER,
                    FAVORITE_COLOR,
                    DATE_OF_BIRTH
            );
    private static final String inputDelimiter = ",";

    @Override
    public String getFormattedPerson(String unformattedPerson, List<Field> outputPropertiesFormat) {
        String unformattedPersonWithoutWhitespace = unformattedPerson.replaceAll("\\s", "");
        List<String> personProperties = split(unformattedPersonWithoutWhitespace, getInputDelimiter());

        personProperties = getPersonPropertiesInOutputFormat(this, personProperties, outputPropertiesFormat);

        return formatPersonPropertiesForOutput(personProperties);
    }

    @Override
    public List<Field> getInputPropertiesFormat() {
        return inputPropertiesFormat;
    }

    @Override
    public String getInputDelimiter() {
        return inputDelimiter;
    }

}
