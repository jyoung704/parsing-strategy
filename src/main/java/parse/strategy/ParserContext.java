package parse.strategy;

import static parse.util.ParseUtils.Field;
import static parse.util.ParseUtils.Field.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ParserContext {
    private final ParseStrategy parseStrategy;
    private final String absoluteFilePath;

    private static final List<Field> outputPropertiesFormat =
            Arrays.asList(
                    FIRST_NAME,
                    LAST_NAME,
                    FAVORITE_COLOR,
                    GENDER,
                    DATE_OF_BIRTH
            );

    public ParserContext(ParseStrategy parseStrategy, String absoluteFilePath) {
        this.parseStrategy = parseStrategy;
        this.absoluteFilePath = absoluteFilePath;
    }

    public void printFormattedPeople() {
        List<String> people = getUnformattedPeople(absoluteFilePath);
        for (String person : people) {
            String formattedPerson = parseStrategy.getFormattedPerson(person, outputPropertiesFormat);

            System.out.println(formattedPerson);
        }
    }

    private static List<String> getUnformattedPeople(String absolutePath) {
        List<String> people = new ArrayList<>();
        try {
            File unformattedPeopleFile = new File(absolutePath);
            Scanner scanner = new Scanner(unformattedPeopleFile);
            while (scanner.hasNextLine()) {
                String data = scanner.nextLine();
                people.add(data);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            String message = String.format("\nabsolutePath = %s", absolutePath);
            System.getLogger("ParseLogger").log(System.Logger.Level.ERROR, e + message);
            return new ArrayList<>();
        }
        return people;
    }
}